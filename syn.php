#!/usr/bin/env php
<?php
#SYN:xcerne01
#title           : syn.php
#author          : Martin Cernek
#email           : xcerne01(at)stud.fit.vutbr.cz
#date            : 12/3/2015
#version         : 1.01
#php_version     : 5.3.3
#usage           : php syn.php [OPTION] [--input=FILE] [--output=FILE] [--format=FILE]
    
    // Control variables.
    $inputFileName = "php://stdin";
    $outputFileName = "php://stdout";
    $formatFileName = "";
    $brOption = false;

    // Parsing of arguments.
    $options = getopt("", array("help::", "br::", "input:", "output:", "format:"));
    // Comparison number of options from parsing and number of arguments from command line.
    if (count($options) == $argc - 1) {
        if ((1 == $argc - 1) && array_key_exists("help", $options)) {
            if ($options["help"]) {
                fwrite(STDERR, "syn.php: invalid options\nTry '--help' for more information.\n");
                exit(1);
            }
            printHelp(); 
            exit(0);
        }
        else {
            // Setting up each control variable.
            foreach ($options as $option => $value) {
                switch ($option) {
                    case "help":
                        fwrite(STDERR, "syn.php: invalid options\nTry '--help' for more information.\n");
                        exit(1);
                    case "br":
                        $brOption = true;
                        if ($value) {
                            fwrite(STDERR, "syn.php: invalid options\nTry '--help' for more information.\n");
                            exit(1);
                        }
                        break;
                    case "input":
                        $inputFileName = $value;
                        break;
                    case "output":
                        $outputFileName = $value;
                        break;
                    case "format":
                        $formatFileName = $value;
                        break;
                    default:
                        break;
                }
            }   
        }
    }
    else {
        fwrite(STDERR, "syn.php: invalid options\nTry '--help' for more information.\n");
        exit(1);
    }
    
    // Reading input file.
    $inputContent = @file_get_contents($inputFileName);
    if (is_bool($inputContent)) {
            fwrite(STDERR, "syn.php: ".$inputFileName.": no such file\n");
            exit(2);
    }

    // Initializing table of format commands.
    $formatTable = array();

    if ($formatFileName) {
        $handleFormatFile = @fopen($formatFileName, "r");
        if ($handleFormatFile) {
            while (!feof($handleFormatFile)) {
                $tmpLine = fgets($handleFormatFile);
                if (($tmpLine == "\n") || (!$tmpLine)) {
                    continue;
                }
                // Every line of format file must match this regular expression.
                if (!preg_match("/^([^\t]*)\t+/", $tmpLine, $tmpRegularExp)) {
                    fwrite(STDERR, "syn.php: ".$formatFileName.": invalid format file\n");
                    fclose($handleFormatFile);
                    exit(4);
                }
                $formatCommands =  preg_replace("/^[^\t]*\t+/", "", $tmpLine);
                // Removing white chars from format commands.
                $formatCommands = preg_replace("/[\t \n]/", "", $formatCommands);
                // Format commands are splitted.
                $formatCommands = preg_split("/,/", $formatCommands);
                // Every command must be from set of available commands.
                foreach ($formatCommands as $formatCommand) {
                    if (!(preg_match("/(^bold$)|(^italic$)|(^underline$)|(^teletype$)|(^size:[1-7]$)|(^color:[0-9a-fA-F]{6,6}$)/", $formatCommand))){
                        fwrite(STDERR, "syn.php: ".$formatFileName.": invalid format command '".$formatCommand."'\n");
                        fclose($handleFormatFile);
                        exit(4);
                    }
                }
                // Convertion of regular format expression.
                $regularExp = convertReg($tmpRegularExp[1]);
                if ($regularExp == 1) {
                    fwrite(STDERR, "syn.php: ".$formatFileName.": invalid regular expression '".$tmpRegularExp[1]."'\n");
                    fclose($handleFormatFile);
                    exit(4);    
                }
                // If regular expression is already in format table other format commands are merged into existing array.
                if (array_key_exists($regularExp, $formatTable)) {
                    $formatTable[$regularExp] = array_merge($formatTable[$regularExp], $formatCommands);
                }   
                else {      
                    $formatTable[$regularExp] = $formatCommands;
                }
            }
            fclose($handleFormatFile);
        }
    }
    
    $outputContent = applyFormat($inputContent, $formatTable);

    // If --br option was set, variable $brOption is true so every char '\n' is replaced by string "<br /\n>".
    if ($brOption) {
        $outputContent = preg_replace("/\n/", "<br />\n", $outputContent);
    }

    // Writing of text into output file
    if (is_bool(@file_put_contents($outputFileName, $outputContent))) {
        fwrite(STDERR, "syn.php: ".$outputFileName.": error in writing to file\n");
        exit(3);
    }




    /*
     *  Function converts format regular expression to regular expression which is supported in PHP 5.
     *
     * @param     format regular expression
     * @return    if success converted regular expression else 1
     */
    function convertReg($regularExp)
    {   
        // Control variables.
        $state = "start";
        $newRegularExp = "";
        $parenCount = 0;
        $negParenCount = 0;
        $isAsterisk = false;
        $lenght = strlen($regularExp);
        $i = 0;

        while ($i < $lenght) {
            switch ($state) {
                case "start":
                    switch ($regularExp[$i]) {
                        case '.':
                            $state = "dot";
                            break;
                        case '%':
                            $state = "percent";
                            $i++;
                            if ($i == $lenght) {
                                return 1;
                            }
                            break;
                        case '(':
                            $state = "leftpar";
                            $newRegularExp .= "(";
                            $parenCount++;
                            $i++;
                            break;
                        case ")":
                            $state = "rightpar";
                            break;
                        case '!':
                            $state = "negation";
                            $i++;
                            if ($i == $lenght) {
                                return 1;
                            }
                            break;
                        case '*':
                            $isAsterisk = true;
                        case '+':
                            if ($i == 0) {
                                return 1;
                            }
                            $state = "quantifier";
                            break;
                        case '^':           // These chars must be escaped.
                        case '$':
                        case '\\':
                        case '{':
                        case '}':
                        case '[':
                        case ']':
                        case '?':
                        case '/':
                            $newRegularExp .= "\\".$regularExp[$i];
                            $state = "start";
                            $i++;
                            break;
                        case '|':
                            $state = "verticalbar";
                            break;
                        default:
                            $newRegularExp .= $regularExp[$i];
                            $state = "start";
                            $i++;
                            break;
                    }
                    break;

                // EXTENSION
                case "quantifier":
                    if ($i == $lenght - 1) {
                        if ($isAsterisk) {
                            $newRegularExp .= '*';
                        }
                        else {
                            $newRegularExp .= '+';
                        }
                        $i++;
                    }
                    else {
                        $i++;
                        if ($regularExp[$i] == '+') {
                            $state = "quantifier";
                        }
                        elseif ($regularExp[$i] == '*') {
                            $isAsterisk = true;
                            $state = "quantifier";
                        }
                        else {
                            if ($isAsterisk) {
                                $newRegularExp .= '*';
                            }
                            else {
                                $newRegularExp .= '+';
                            }
                            $isAsterisk = false;
                            $state = "start";
                        }
                    }
                    break;

                case "dot":
                    if (($i == 0) || ($i == $lenght - 1) || ($regularExp[$i+1] == '+') || ($regularExp[$i+1] == '*') || ($regularExp[$i+1] == '.') || ($regularExp[$i+1] == '|')) {
                        return 1;
                    }
                    $state = "start";
                    $i++;
                    break;

                case "verticalbar":
                    if (($i == 0) || ($i == $lenght - 1) || ($regularExp[$i+1] == '+') || ($regularExp[$i+1] == '*') || ($regularExp[$i+1] == '.') || ($regularExp[$i+1] == '|')) {
                        return 1;
                    }
                    $newRegularExp .= "|";
                    $state = "start";
                    $i++;
                    break;

                case "percent":
                    switch ($regularExp[$i]) {
                        case "s":
                            $newRegularExp .= "[ \\t\\n\\r\\f\\v]";
                            break;
                        case "d":
                            $newRegularExp .= "[0-9]";
                            break;
                        case "l":
                            $newRegularExp .= "[a-z]";
                            break;
                        case "L":
                            $newRegularExp .= "[A-Z]";
                            break;
                        case "w":
                            $newRegularExp .= "[a-zA-Z]";
                            break;
                        case "W":
                            $newRegularExp .= "[a-zA-Z0-9]";
                            break;
                        case "a":
                            $newRegularExp .= ".";
                            break;
                        case "t":           // These chars must be escaped.
                        case "n":
                        case ".":
                        case "|":
                        case "!":
                        case "*":
                        case "+":
                        case "(":
                        case ")":
                        case "%":
                            $newRegularExp .= "\\".$regularExp[$i];
                            break;
                        default:
                            return 1;
                    }
                    $state = "start";
                    $i++;
                    break;

                case 'leftpar':
                    if ($regularExp[$i] == ")") {
                        return 1;
                    }
                    $state = "start";
                    break;

                case "rightpar":
                    if ($parenCount == 0) {
                        return 1;
                    }
                    $parenCount--;
                    $newRegularExp .= ")";
                    $state = "start";
                    $i++;
                    break;

                case "negation":
                    switch ($regularExp[$i]) {
                        case '+':           // These chars must not be after char '!'.
                        case '*':
                        case '!':
                        case '|':
                        case ')':
                        case '.':
                            return 1;
                            break;
                        case '^':           // These chars must be escaped.
                        case '$':
                        case '\\':
                        case '{':
                        case '}':
                        case '[':
                        case ']':
                        case '?':
                        case '/':
                            $newRegularExp .= "[^\\".$regularExp[$i]."]";
                            $state = "start";
                            $i++;
                            break;
                        case '%':
                            $state = "negpercent";
                            $i++;
                            if ($i == $lenght) {
                                return 1;
                            }
                            break;
                        case '(':
                            $state = "negleftpar";
                            $negParenCount++;
                            $i++;
                            if ($i == $lenght) {
                                return 1;
                            }
                            if ($regularExp[$i] == ")"){
                                return 1;
                            }
                            $newRegularExp .= "[^";
                            break;
                        default:
                            $newRegularExp .= "[^".$regularExp[$i]."]";
                            $state = "start";
                            $i++;
                            break;
                    }
                    break;

                case "negpercent":
                    switch ($regularExp[$i]) {
                        case "s":
                            $newRegularExp .= "[^ \\t\\n\\r\\f\\v]";
                            break;
                        case "d":
                            $newRegularExp .= "[^0-9]";
                            break;
                        case "l":
                            $newRegularExp .= "[^a-z]";
                            break;
                        case "L":
                            $newRegularExp .= "[^A-Z]";
                            break;
                        case "w":
                            $newRegularExp .= "[^a-zA-Z]";
                            break;
                        case "W":
                            $newRegularExp .= "[^a-zA-Z0-9]";
                            break;
                        case "a":
                            return $newRegularExp = "";
                            break;
                        case "t":           // These chars must be escaped.
                        case "n":
                        case ".":
                        case "|":
                        case "!":
                        case "*":
                        case "+":
                        case "(":
                        case ")":
                        case "%":
                            $newRegularExp .= "[^\\".$regularExp[$i]."]";
                            break;
                        default:
                            return 1;
                    }
                    $state = "start";
                    $i++;
                    break;

                case "negleftpar":
                    switch ($regularExp[$i]) {
                        case '+':
                        case '*':
                        case '!':
                            return 1;
                        case ')':
                            $negParenCount--;
                            if ($negParenCount < 0) {
                                return 1;
                            }
                            elseif ($negParenCount == 0) {
                                $newRegularExp .= "]";
                                $state = "start";
                                $i++;
                            }
                            $state = "negleftpar";
                            $i++;
                            if ($i == $lenght) {
                                return 1;
                            }
                            break;
                        case '|':
                            if ($negParenCount == 0) {
                                return 1;
                            }
                            $state = "negleftpar";
                            $i++;
                            if ($i == $lenght) {
                                return 1;
                            }
                            if ($regularExp[$i] == ')') {
                                return 1;
                            }
                            break;
                        case '(':
                            $state = "negleftpar";
                            $negParenCount++;
                            $i++;
                            if ($i == $lenght) {
                                return 1;
                            }
                            break;
                        case '%':
                            $state = "negleftparpercent";
                            $i++;
                            if ($i == $lenght) {
                                return 1;
                            }
                            break;
                        default:
                            $state = "negend";
                            $newRegularExp .= $regularExp[$i];
                            $i++;
                            if ($i == $lenght) {
                                return 1;
                            }
                            break;
                    }
                    break;

                case "negend":
                    switch ($regularExp[$i]) {
                        case ')':
                            $negParenCount--;
                            if ($negParenCount == 0) {
                                $newRegularExp .= "]";
                                $state = "start";
                                $i++;   
                            }
                            else {
                                $state = "negleftpar";
                                $i++;
                                if ($i == $lenght) {
                                    return 1;
                                }
                            }
                            break;
                        case '|':
                            $state = "negleftpar";
                            $i++;
                            if ($i == $lenght) {
                                return 1;
                            }
                            break;
                        default:
                            return 1;
                            break;
                    }
                    break;

                case "negleftparpercent":
                    switch ($regularExp[$i]) {
                        case "s":
                            $newRegularExp .= " \\t\\n\\r\\f\\v";
                            break;
                        case "d":
                            $newRegularExp .= "0-9";
                            break;
                        case "l":
                            $newRegularExp .= "a-z";
                            break;
                        case "L":
                            $newRegularExp .= "A-Z";
                            break;
                        case "w":
                            $newRegularExp .= "a-zA-Z";
                            break;
                        case "W":
                            $newRegularExp .= "a-zA-Z0-9";
                            break;
                        case "a":
                            return $newRegularExp = "";
                            break;
                        case "t":           // These chars must be escaped.
                        case "n":
                        case ".":
                        case "|":
                        case "!":
                        case "*":
                        case "+":
                        case "(":
                        case ")":
                        case "%":
                            $newRegularExp .= "\\".$regularExp[$i];
                            break;
                        default:
                            return 1;
                    }
                    $state = "negend";
                    $i++;
                    if ($i == $lenght) {
                        return 1;
                    }
                    break;
            }
        }

        // Number of pairs of parentheses must be 0.
        if ($parenCount != 0) {
            return 1;
        }

        return $newRegularExp;
    }

    /*
     *  Function converts format regular expression to regular expression which is supported in PHP 5.
     *
     * @param     input text
     * @param     table of format commands
     * @return    text with html tags or if $formatTable is empty input text is returned
     */
    function applyFormat($inputContent, $formatTable)
    {   
        // Initializaion of output text.
        $outputContent = "";

        if (empty($formatTable)) {
            return $inputContent;
        }

        // Searching for every match for regular expression from format table.
        foreach ($formatTable as $regularExp => $value) {
            preg_match_all("/$regularExp/s", $inputContent, $matches[$regularExp], PREG_OFFSET_CAPTURE);
        }

        // Calculating of ending indexes.
        foreach ($matches as $key => $value) {
            for ($i = 0; $i < count($value[0]); $i++) { 
                $value[0][$i][2] = $value[0][$i][1] + strlen($value[0][$i][0]) - 1;
            }
            $matches[$key] = $value[0];
        }

        for ($i=0; $i < strlen($inputContent); $i++) { 
            
            // Addition of opening tags.
            foreach ($matches as $key => $value) {
                for ($j = 0; $j < count($value); $j++) {
                    if (strlen($value[$j][0]) == 0) {
                        continue;
                    }
                    if ($i == $value[$j][1]) {
                        foreach($formatTable[$key] as $format) {
                            if ($format == "bold") {
                                $outputContent .= "<b>";
                            }
                            elseif ($format == "italic") {
                                $outputContent .= "<i>";
                            }
                            elseif ($format == "underline") {
                                $outputContent .= "<u>";
                            }
                            elseif ($format == "teletype") {
                                $outputContent .= "<tt>";
                            }
                            elseif (preg_match("/^size:([1-7])$/", $format, $size)) {
                                $outputContent .= "<font size=$size[1]>";
                            }
                            elseif (preg_match("/^color:([0-9a-fA-F]{6,6})$/", $format, $color)) {
                                $outputContent .= "<font color=#$color[1]>";
                            }
                        }
                    }
                }
            }

            // Addition of char from actual position.
            $outputContent .= $inputContent[$i];
            
            // Addition of closing tags in reverse order.
            $reverseMatches = array_reverse($matches);
            foreach ($reverseMatches as $key => $value) {
                for ($j = 0; $j < count($value); $j++) {
                    if (strlen($value[$j][0]) == 0) {
                        continue;
                    }
                    if ($i == $value[$j][2]) {
                        foreach(array_reverse($formatTable[$key]) as $format) {
                            if ($format == "bold") {
                                $outputContent .= "</b>";
                            }
                            elseif ($format == "italic") {
                                $outputContent .= "</i>";
                            }
                            elseif ($format == "underline") {
                                $outputContent .= "</u>";
                            }
                            elseif ($format == "teletype") {
                                $outputContent .= "</tt>";
                            }
                            elseif ((preg_match("/^size:([1-7])$/", $format)) || (preg_match("/^color:([0-9a-fA-F]{6,6})$/", $format))) {
                                $outputContent .= "</font>";
                            }
                        }
                    }
                }
            }
        }

        return $outputContent;
    }

    /*
     *  Function print help on the standard output.
     */
    function printHelp()
    {
        echo "Usage: php syn.php [OPTION] [--input=FILE] [--output=FILE] [--format=FILE]\n".
             "Apply html tag into pattern from format file. Pattern is a regular expression.\n\n".
             "Option:\n".
             "  --br           add tag <br /> in the end of each line\n".
             "  --help         print help\n\n".
             "Standart streams are used without options --input and --output.\n".
             "Without option --format output will be the same as input.\n\n".
             "For more information see documentation.\n".
             "  (c) xcerne01@stud.fit.vutbr.cz\n";
    }

?>
